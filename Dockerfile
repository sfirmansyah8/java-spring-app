# Custom base image java with Fedora OS and has been scanned using snyk
FROM openjdk:25-slim-bullseye

# Check versionjava & set timezone
RUN java -version \
    && cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime \
    && echo "Asia/Jakarta" > /etc/timezone \
    && mkdir /apps 

# Initialization directory
WORKDIR /apps

# create user and set ownership and permissions
RUN adduser -D adm-app \
    && chown -R adm-app /apps
USER adm-app

# Build Application
RUN ./mvnw clean package
ls

# Expose Port Application
EXPOSE 8080

# Copy File Jar
COPY ./target/spring-petclinic*.jar  /apps/

# Running Java Application With Kibana
ENTRYPOINT java \ 
           -Xms128m \ 
           -Xmx256m \ 
           -jar /apps/spring-petclinic*.jar